<?php

namespace Drupal\Tests\something_went_wrong\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Class Something Went Wrong Service Test.
 *
 * @group something_went_wrong
 */
class SomethingWentWrongServiceTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'something_went_wrong',
  ];

  /**
   * Tests the build message method.
   */
  public function testBuildMessage() {
    /** @var \Drupal\something_went_wrong\SomethingWentWrongServiceInterface $something_went_wrong_service */
    $something_went_wrong_service = \Drupal::service('something_went_wrong.service');
    $exception = new \Exception('Internal Server Error', 500);
    $message = $something_went_wrong_service->buildMessage($exception);
    $reflector = new \ReflectionClass($this);
    $class_file_path = $reflector->getFileName();
    $this->assertEquals("*Uncaught PHP Exception* \n Class name: Exception \n Message: Internal Server Error \n File: $class_file_path \n Line: 29", $message);
  }

}
