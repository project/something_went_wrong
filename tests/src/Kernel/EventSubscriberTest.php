<?php

namespace Drupal\Tests\something_went_wrong\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Class Event Subscriber Test.
 *
 * @group something_went_wrong
 */
class EventSubscriberTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'something_went_wrong',
  ];

  /**
   * Tests the event subscriber class name.
   */
  public function testClassName() {
    $definition = $this->container->getDefinition('something_went_wrong.exception.subscriber');
    if (floatval(\Drupal::VERSION) >= 9) {
      $this->assertEquals('Drupal\something_went_wrong\EventSubscriber\SomethingWentWrongD9ExceptionSubscriber', $definition->getClass());
    }
    else {
      $this->assertEquals('Drupal\something_went_wrong\EventSubscriber\SomethingWentWrongExceptionSubscriber', $definition->getClass());
    }
  }

}
