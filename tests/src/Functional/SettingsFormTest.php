<?php

namespace Drupal\Tests\something_went_wrong\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the settings form.
 *
 * @group something_went_wrong
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'something_went_wrong',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $account = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($account);
  }

  /**
   * Tests form structure.
   */
  public function testFormStructure() {
    $this->drupalGet('admin/config/something-went-wrong/settings');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldExists('label');
    $this->assertSession()->fieldExists('slack');
    $this->assertSession()->fieldExists('mail');
    $this->assertSession()->fieldExists('ignore_list');
    $this->assertSession()->titleEquals('Something Went Wrong | Drupal');
    $this->assertSession()->buttonExists('Save configuration');
  }

  /**
   * Tests if configuration is possible.
   */
  public function testConfiguration() {
    $this->assertEmpty($this->config('something_went_wrong.settings')->get('label'));
    $this->assertEmpty($this->config('something_went_wrong.settings')->get('slack'));
    $this->assertEmpty($this->config('something_went_wrong.settings')->get('webhook_url'));
    $this->assertEmpty($this->config('something_went_wrong.settings')->get('mail'));
    $this->assertEmpty($this->config('something_went_wrong.settings')->get('mail_custom_address'));
    $this->assertEmpty($this->config('something_went_wrong.settings')->get('ignore_list'));

    $this->drupalGet('admin/config/something-went-wrong/settings');
    $edit['label'] = 'Test site';
    $edit['slack'] = TRUE;
    $edit['webhook_url'] = 'www.example.com';
    $edit['mail'] = TRUE;
    $edit['mail_custom_address'] = 'test@example.com';
    $edit['ignore_list'] = 'IgnoreClass';
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->responseContains('The configuration options have been saved.');

    $this->assertEquals('Test site', $this->config('something_went_wrong.settings')->get('label'));
    $this->assertTrue($this->config('something_went_wrong.settings')->get('slack'));
    $this->assertEquals('www.example.com', $this->config('something_went_wrong.settings')->get('webhook_url'));
    $this->assertTrue($this->config('something_went_wrong.settings')->get('mail'));
    $this->assertEquals('test@example.com', $this->config('something_went_wrong.settings')->get('mail_custom_address'));
    $this->assertEquals('IgnoreClass', $this->config('something_went_wrong.settings')->get('ignore_list'));
  }

  /**
   * Tests form access for anonymous users.
   */
  public function testFormAccess() {
    $this->drupalLogout();
    $this->drupalGet('admin/config/something-went-wrong/settings');
    $this->assertSession()->statusCodeEquals(403);
  }

}
