CONTENTS OF THIS FILE
----------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module catches PHP exceptions and sends them to your Slack channel or 
email.


REQUIREMENTS
------------

None.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

Please go to Configuration > Development > Something Went Wrong to configure the
Something Went Wrong module. 


MAINTAINERS
-----------

Current maintainers:
 * Bojan M. (bojanm) - https://www.drupal.org/u/bojanm
 * Goran Nikolovski (gnikolovski) - https://www.drupal.org/u/gnikolovski
