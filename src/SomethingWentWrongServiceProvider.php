<?php

namespace Drupal\something_went_wrong;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Service Provider for Something Went Wrong.
 *
 * @package Drupal\something_went_wrong
 */
class SomethingWentWrongServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if (floatval(\Drupal::VERSION) >= 9) {
      $definition = $container->getDefinition('something_went_wrong.exception.subscriber');
      $definition->setClass('Drupal\something_went_wrong\EventSubscriber\SomethingWentWrongD9ExceptionSubscriber');
    }
  }

}
