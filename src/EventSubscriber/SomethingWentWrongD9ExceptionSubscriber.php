<?php

namespace Drupal\something_went_wrong\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\something_went_wrong\SomethingWentWrongServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class Something Went Wrong D9 Exception Subscriber.
 *
 * @package Drupal\something_went_wrong\EventSubscriber
 */
class SomethingWentWrongD9ExceptionSubscriber implements EventSubscriberInterface {

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $moduleConfig;

  /**
   * The Something Went Wrong service.
   *
   * @var \Drupal\something_went_wrong\EventSubscriber\SomethingWentWrongServiceInterface
   */
  protected $somethingWentWrongService;

  /**
   * SomethingWentWrongD9ExceptionSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\something_went_wrong\SomethingWentWrongServiceInterface $something_went_wrong_service
   *   The Something Went Wrong service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    SomethingWentWrongServiceInterface $something_went_wrong_service
  ) {
    $this->moduleConfig = $config_factory->get('something_went_wrong.settings');
    $this->somethingWentWrongService = $something_went_wrong_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::EXCEPTION][] = ['onException', 60];
    return $events;
  }

  /**
   * Exceptions handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The event to process.
   */
  public function onException(ExceptionEvent $event) {
    $exception = $event->getThrowable();
    $class_name = get_class($exception);
    $ignore_list = explode(',', $this->moduleConfig->get('ignore_list'));

    $send = TRUE;

    foreach ($ignore_list as $item) {
      if ($item && strpos($class_name, trim($item)) !== FALSE) {
        $send = FALSE;
        break;
      }
    }

    if ($send) {
      $message = $this->somethingWentWrongService->buildMessage($exception);

      if ($this->moduleConfig->get('slack')) {
        $this->somethingWentWrongService->sendMessageToSlack($message);
      }

      if ($this->moduleConfig->get('mail')) {
        $this->somethingWentWrongService->sendMessageToMail($message);
      }
    }
  }

}
