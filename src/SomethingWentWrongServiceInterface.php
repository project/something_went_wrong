<?php

namespace Drupal\something_went_wrong;

/**
 * Interface Something Went Wrong Service Interface.
 *
 * @package Drupal\something_went_wrong
 */
interface SomethingWentWrongServiceInterface {

  /**
   * Builds message.
   *
   * @param \Exception $exception
   *   The exception.
   *
   * @return string
   *   The message.
   */
  public function buildMessage(\Exception $exception);

  /**
   * Sends message to Slack.
   *
   * @param string $message
   *   The message.
   */
  public function sendMessageToSlack($message);

  /**
   * Sends message to mail.
   *
   * @param string $message
   *   The message.
   */
  public function sendMessageToMail($message);

}
